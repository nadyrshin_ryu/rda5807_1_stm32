//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include "delay.h"
#include "buttmatrix.h"
#include "gpio.h"
#include "i2cm.h"


#if (BUTTMATRIX_pcf8574_mode)
  #include "pcf8574.h"
  uint8_t buttmatrix_pcf8574_Value = 0;
#endif

  
// ������� ��� ������ �������� ����� GPIO ���������������� 
#define BUTTMATRIX_SetCols_PinMode_In(mask)   gpio_SetGPIOmode_In(BUTTMATRIX_ColsPort, mask, gpio_PullUp)
#define BUTTMATRIX_SetRows_PinMode_In(mask)   gpio_SetGPIOmode_In(BUTTMATRIX_RowsPort, mask, gpio_PullUp)
#define BUTTMATRIX_SetCols_PinMode_Out(mask)  gpio_SetGPIOmode_Out(BUTTMATRIX_ColsPort, mask)
#define BUTTMATRIX_SetRows_PinMode_Out(mask)  gpio_SetGPIOmode_Out(BUTTMATRIX_RowsPort, mask)
#define BUTTMATRIX_ColsPin_SET(val)           {GPIO_ResetBits(BUTTMATRIX_ColsPort, BUTTMATRIX_Cols_Mask); GPIO_SetBits(BUTTMATRIX_ColsPort, val);}
#define BUTTMATRIX_RowsPin_SET(val)           {GPIO_ResetBits(BUTTMATRIX_RowsPort, BUTTMATRIX_Rows_Mask); GPIO_SetBits(BUTTMATRIX_RowsPort, val);}
#define BUTTMATRIX_ColsPin_GET()              GPIO_ReadInputData(BUTTMATRIX_ColsPort)
#define BUTTMATRIX_RowsPin_GET()              GPIO_ReadInputData(BUTTMATRIX_RowsPort)


const uint16_t RowMasks[] = {BUTTMATRIX_Row0_Mask, BUTTMATRIX_Row1_Mask, BUTTMATRIX_Row2_Mask, BUTTMATRIX_Row3_Mask};
const uint16_t ColMasks[] = {BUTTMATRIX_Col0_Mask, BUTTMATRIX_Col1_Mask, BUTTMATRIX_Col2_Mask, BUTTMATRIX_Col3_Mask};

//==============================================================================
// ������� ��������� ��������� ������ ���������� � ���������� ��� � ���� ������� 16-��� �����
//==============================================================================
uint16_t buttmatrix_scan(void)
{
  uint16_t ReturnValue = 0;
  uint16_t ButtonsMask = 1;

  BUTTMATRIX_SetCols_PinMode_In(BUTTMATRIX_Cols_Mask);

  for (uint8_t row = 0; row < (sizeof(RowMasks) / sizeof(RowMasks[0])); row++)
  {
    uint16_t RowMask = RowMasks[row];
    
    BUTTMATRIX_SetRows_PinMode_In(BUTTMATRIX_Rows_Mask);
    BUTTMATRIX_SetRows_PinMode_Out(RowMask);
    
    BUTTMATRIX_RowsPin_SET(~RowMask);
    uint16_t Cols = BUTTMATRIX_ColsPin_GET() & BUTTMATRIX_Cols_Mask;
    
    for (uint8_t col = 0; col < (sizeof(ColMasks) / sizeof(ColMasks[0])); col++)
    {
      if (!(Cols & ColMasks[col]))
        ReturnValue |= ButtonsMask;
      
      ButtonsMask <<= 1;
    }
  }
  
  return ReturnValue;
}
//==============================================================================


//==============================================================================
// ��������� ������������� ��� ��� ������ � �����������
//==============================================================================
void buttmatrix_init(void)
{
  // �������� ������������ ��������� �����
  gpio_PortClockStart(BUTTMATRIX_ColsPort);
  gpio_PortClockStart(BUTTMATRIX_RowsPort);
}
//==============================================================================
