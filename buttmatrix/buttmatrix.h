//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _BUTTMATRIX_H
#define _BUTTMATRIX_H

#include "../types.h"


// ���� ����������������, � ����� �������� ���������� ����� ��������
#define BUTTMATRIX_ColsPort           GPIOA
// ���� ����������������, � ����� �������� ���������� ����� �����
#define BUTTMATRIX_RowsPort           GPIOB


// ������� ����� �����
#define BUTTMATRIX_Col0_Mask            (1 << 8)
#define BUTTMATRIX_Col1_Mask            (1 << 9)
#define BUTTMATRIX_Col2_Mask            (1 << 10)
#define BUTTMATRIX_Col3_Mask            (1 << 11)
#define BUTTMATRIX_Row0_Mask            (1 << 12)
#define BUTTMATRIX_Row1_Mask            (1 << 13)
#define BUTTMATRIX_Row2_Mask            (1 << 14)
#define BUTTMATRIX_Row3_Mask            (1 << 15)

#define BUTTMATRIX_Rows_Mask            (BUTTMATRIX_Row0_Mask | BUTTMATRIX_Row1_Mask | BUTTMATRIX_Row2_Mask | BUTTMATRIX_Row3_Mask)
#define BUTTMATRIX_Cols_Mask            (BUTTMATRIX_Col0_Mask | BUTTMATRIX_Col1_Mask | BUTTMATRIX_Col2_Mask | BUTTMATRIX_Col3_Mask)




// ������� ��������� ��������� ������ ���������� � ���������� ��� � ���� ������� 16-��� �����
uint16_t buttmatrix_scan(void);
// ��������� ������������� ��� ��� ������ � �����������
void buttmatrix_init(void);


#endif