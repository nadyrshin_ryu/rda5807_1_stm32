//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"
#include "max7219.h"


#define MAX7219_CS_HIGH()       GPIO_WriteBit(MAX7219_CS_Port, MAX7219_CS_Pin, Bit_SET)
#define MAX7219_CS_LOW()        GPIO_WriteBit(MAX7219_CS_Port, MAX7219_CS_Pin, Bit_RESET)


//==============================================================================
// ��������� �������������� �����, ������������ ����������� ������ � max7219
//==============================================================================
void max7219_GPIO_init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStruct.GPIO_Pin = (1 << 5) | (1 << 7);
  GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStruct.GPIO_Pin = MAX7219_CS_Pin;
  GPIO_Init(MAX7219_CS_Port, &GPIO_InitStruct);
 
  MAX7219_CS_HIGH();
}
//==============================================================================


//==============================================================================
// ��������� �������������� SPI
//==============================================================================
void max7219_SPI_init(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
  
  SPI_InitTypeDef SPI_InitStruct;
  
  SPI_InitStruct.SPI_Direction = SPI_Direction_1Line_Tx;
  SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
  SPI_InitStruct.SPI_DataSize = SPI_DataSize_16b;
  SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;//16;
  SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStruct.SPI_CRCPolynomial = 0x7;
    
  SPI_Init(SPI1, &SPI_InitStruct);
  SPI_Cmd(SPI1, ENABLE);
}
//==============================================================================


//==============================================================================
// ��������� �������������� ��������� ������ � �������� max7219
//==============================================================================
void max7219_init(void)
{
  max7219_SPI_init();
  max7219_GPIO_init();
}
//==============================================================================


//==============================================================================
// ��������� ���������� ������� � ������ � ���� ��� �� ��� max7219 � �������
//==============================================================================
void max7219_send(uint8_t MAX_Idx, uint8_t Cmd, uint8_t Data)
{
  uint16_t max7219_SpiBuff[MAX7219_NUM];
  uint16_t Word = Data | ((uint16_t) Cmd << 8);
  
  for (uint8_t i = 0; i < MAX7219_NUM; i++)
  {
    if (MAX_Idx == 0xFF)  // ����� �������� �� ��� max7219 � �������
      max7219_SpiBuff[i] = Word;
    else                  // ����� �������� ������ � ���� max7219
    {
      if (i == MAX_Idx)         // �� ���������� max7219, � ������� ����� �������� �������/������
        max7219_SpiBuff[i] = Word;
      else                      // max7219, �������� ��� ������ �� ������
        max7219_SpiBuff[i] = 0x00 | ((uint16_t) MAX7219_CMD_NO_OP << 8);
    }
  }
  
  // ����� ����� ���� �� ��� ������������ max7219
  max7219_sendarray(max7219_SpiBuff);
}
//==============================================================================


//==============================================================================
// ��������� ���������� ������ ������ � max7219
//==============================================================================
void max7219_sendarray(uint16_t *pArray)
{
  MAX7219_CS_LOW();
  
  for (uint8_t i = 0; i < MAX7219_NUM; i++)
    max7219_send16bit(*(pArray++));
  
  MAX7219_CS_HIGH();
}
//==============================================================================


//==============================================================================
// ��������� ���������� 16-������ ����� �� SPI
//==============================================================================
void max7219_send16bit(uint16_t Word)
{
  SPI_I2S_SendData(SPI1, Word);
  while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET)  {}
}
//==============================================================================


//==============================================================================
// ��������� ������������� ����� ������������� �������� � 1 ��� �� ���� max7219
//==============================================================================
void max7219_set_decodemode(uint8_t MAX_Idx, uint8_t DecodeMode)
{
  max7219_send(MAX_Idx, MAX7219_CMD_DECODE_MODE, DecodeMode);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ������� � 1 ��� �� ���� max7219
//==============================================================================
void max7219_set_intensity(uint8_t MAX_Idx, uint8_t Intensity)
{
  if (Intensity > 15)
    Intensity = 15;
  
  max7219_send(MAX_Idx, MAX7219_CMD_INTENSITY, Intensity);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ���-�� ������/����� � 1 ��� �� ���� max7219
//==============================================================================
void max7219_set_scan_limit(uint8_t MAX_Idx, uint8_t Limit)
{
  if (Limit > 7)
    Limit = 7;
  
  max7219_send(MAX_Idx, MAX7219_CMD_SCAN_LIMIT, Limit);
}
//==============================================================================


//==============================================================================
// ��������� ��������/��������� max7219. ����� ������ ������� �� ��������
//==============================================================================
void max7219_set_run_onoff(uint8_t MAX_Idx, uint8_t On)
{
  if (On)
    On = 1;
  
  max7219_send(MAX_Idx, MAX7219_CMD_SHUTDOWN, On);
}
//==============================================================================


//==============================================================================
// ��������� ��������/��������� �������� ����� max7219 (����� ��� ����������)
//==============================================================================
void max7219_set_testmode_onoff(uint8_t MAX_Idx, uint8_t On)
{
  if (On)
    On = 1;
  
  max7219_send(MAX_Idx, MAX7219_CMD_DISP_TEST, On);
}
//==============================================================================
