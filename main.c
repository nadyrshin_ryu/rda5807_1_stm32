//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "stm32f10x.h"
#include "main.h"
#include "rda5807.h"
#include "buttmatrix.h"
#include "delay.h"
#include "max7219.h"
#include "ledmatrix.h"
#include "max7219\fonts\font.h"


I2C_TypeDef* I2Cx = I2C1;               // I2C, ������� ������������ � ������� ��� ����������� rda5807
uint8_t Volume = 5;                     // ���������
uint8_t BassBoost = 0;                  // ��������� BassBoost
uint16_t Freq100hz = 0;                 // �������� ������� ��� ����� (��� ����� � ����������)

// ��� ����������� ��������� ��������� �����
typedef enum 
{
  Idle = 0,
  FreqSet = 1,
  SeekTune = 2,
  VolSet = 3,
  BassSet = 4
} tRadioState;

tRadioState RadioState = Idle;          // ������� ��������� �����
tRadioState RadioStateOld = Idle;       // ������� ��������� �����
uint16_t StateTickNum;                  // ������� ������� �������� ���������


//==============================================================================
// ������� �� ������� � ������� ����� ������� ������ ���������� ����� ������, 
// ������� ������ ��� ���� ������
//==============================================================================
uint16_t ButtonsPush(uint16_t Buttons, uint16_t ButtonsOld)
{
  return Buttons & (ButtonsOld ^ 0xFFFF);
}
//==============================================================================


//==============================================================================
// ��������� ������ ������� ��������� ����� � ������������� ����� ���������
// � ����� ��������� ����� � MAIN 
//==============================================================================
void RadioNewState(tRadioState NewState, uint16_t TickNum)
{
  RadioStateOld = (NewState == Idle) ? Idle : RadioState;
  RadioState = NewState;
  StateTickNum = TickNum;
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ��������� ��������� � ������� Vol x
//==============================================================================
void ShowVol(uint8_t vol)
{
  ledmatrix_fill_screenbuff(0);
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "Vol %d", vol);
  ledmatrix_update_from_buff();
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ��������� ��������� BassBoost � ������� Bass x ���
// BB x (����� ������� � 4 ������������ �������)
//==============================================================================
void ShowBass(uint8_t val)
{
  ledmatrix_fill_screenbuff(0);

#if (MAX7219_NUM == 5)
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "Bass %d", val);
#else 
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "BB %d", val);
#endif
  
  ledmatrix_update_from_buff();
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ��������� �������� freq � ������� xxx.x
//==============================================================================
void ShowFreq(uint16_t freq)
{
  ledmatrix_fill_screenbuff(0);
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "%d.%d", freq / 10, freq % 10);
  ledmatrix_update_from_buff();
}
//==============================================================================


//==============================================================================
// ��������� ������ �������� Freq100hz (�������� ������� ��� �����) 
// � ������������ � ����� �������� ������ 0-9
//==============================================================================
void EnterFreqDigit(uint8_t Digit)
{
  // �������� ����� ������� ������� � ���������� ���� ����� �� 1 ����� 
  // � � ������� ������ ��������� ����� �����
  uint16_t TempFreq = (Freq100hz * 10) + Digit;
  
  // � ������ ����� ����� ������� �� ������� ������������ �������
  if (TempFreq > 1080)
    TempFreq = TempFreq % 1000;     // ����������� �� ������ ������ �������
  
  Freq100hz = TempFreq;
}
//==============================================================================


//==============================================================================
// ��������� ������������ ������� ������ �� ����� ������� ������ (0->1)
//==============================================================================
void ButtonClickProcess(uint16_t ButtonClick)
{
  // ���� ������ ������ +
  if (ButtonClick & (1 << 1))
  {
    // ����������� ���������
    if (Volume <= 15)
      rda5807_SetVolume(I2C1, ++Volume);
    RadioNewState(VolSet, 10);
    ShowVol(Volume);
  }
  // ���� ������ ������ -
  if (ButtonClick & (1 << 0))
  {
    // ��������� ���������
    if (Volume)
      rda5807_SetVolume(I2C1, --Volume);
    RadioNewState(VolSet, 10);
    ShowVol(Volume);
  }
  // ���� ������ ������ <
  if (ButtonClick & (1 << 3))
  {
    // �������� ����� ������������ ����
    rda5807_StartSeek(I2C1, 1);
    RadioNewState(SeekTune, 100);
  }
  // ���� ������ ������ >
  if (ButtonClick & (1 << 2))
  {
    // �������� ����� ������������ �����
    rda5807_StartSeek(I2C1, 0);
    RadioNewState(SeekTune, 100);
  }
  
  // ���� ������ ������ 0
  if (ButtonClick & (1 << 8))
  {
    EnterFreqDigit(0);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 1
  if (ButtonClick & (1 << 15))
  {
    EnterFreqDigit(1);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 2
  if (ButtonClick & (1 << 11))
  {
    EnterFreqDigit(2);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 3
  if (ButtonClick & (1 << 7))
  {
    EnterFreqDigit(3);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 4
  if (ButtonClick & (1 << 14))
  {
    EnterFreqDigit(4);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 5
  if (ButtonClick & (1 << 10))
  {
    EnterFreqDigit(5);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 6
  if (ButtonClick & (1 << 6))
  {
    EnterFreqDigit(6);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 7
  if (ButtonClick & (1 << 13))
  {
    EnterFreqDigit(7);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 8
  if (ButtonClick & (1 << 9))
  {
    EnterFreqDigit(8);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 9
  if (ButtonClick & (1 << 5))
  {
    EnterFreqDigit(9);
    RadioNewState(FreqSet, 30);
  }
  
  // ���� ������ ������ *
  if (ButtonClick & (1 << 12))
  {
    // ������ ��������� ������� BassBoost
    if (Freq100hz)      // ���� �������� �������� ������� ���� ����� ������� 
    {
      // ���������� ������������� rda5807 �� ����� �������
      rda5807_SetFreq_In100Khz(I2C1, Freq100hz);
      Freq100hz = 0;
      RadioNewState(SeekTune, 100);
    }
  }

  // ���� ������ ������ #
  if (ButtonClick & (1 << 4))
  {
    // ������ ��������� ������� BassBoost
    BassBoost ^= 0x01;
    rda5807_SetBassBoost(I2C1, BassBoost);
    RadioNewState(BassSet, 10);
    ShowBass(BassBoost);
  }
}
//==============================================================================


//==============================================================================
// MAIN
//==============================================================================
void main()
{
  uint16_t Buttons = 0, ButtonsOld = 0;
  uint16_t FreqRead;
  
  SystemInit();

  // �������������� ������������ �������
  delay_ms(200);
  buttmatrix_init();
  ledmatrix_init();
  rda5807_init(I2Cx);
  //rda5807_SoftReset(I2Cx);    // ������� ����� �����, � ����� ������� ������??
  
  // ������ ����� ��������� (���� ���� �����������)
  ledmatrix_testmatrix(1);
  delay_ms(500);
  ledmatrix_testmatrix(0);

  // ������������� ��������� rda5807 ��-���������
  rda5807_SetupDefault(I2Cx);
  rda5807_SetVolume(I2Cx, Volume);
  rda5807_SetBassBoost(I2Cx, BassBoost);

  delay_ms(100);

  // ������������� ��������� �������
  rda5807_SetFreq_In100Khz(I2Cx, 1012);
  // ��������� � ��������� �������� ������/��������� �������
  RadioNewState(SeekTune, 100);

  while (1)
  {
    // ��������� ���������� � ���������� ������ ������� �� ������
    ButtonsOld = Buttons;
    Buttons = buttmatrix_scan();
    uint16_t ButtonClick = ButtonsPush(Buttons, ButtonsOld);
    ButtonClickProcess(ButtonClick);

    // ������������ ������ �������� ��������� ����� (RadioState)
    switch (RadioState)
    {
      // ��������� �������� (�������������)
    case Idle:
      break;
      // ��������� ����� �� ����� ����������� ���������
    case VolSet:
      break;
      // ��������� ����� �� ����� ���������/���������� BassBoost
    case BassSet:
      break;
      // ��������� ����� �� ����� ����� � ���������� �������
    case FreqSet:
      ShowFreq(Freq100hz);
      break;
      // ��������� ����� �� ����� ������ ������������
    case SeekTune:
      // ���� Seek ��� Tune � �������� - ������� �� ��������� ������� �������
      if (rda5807_Get_SeekTuneReadyFlag(I2Cx))
        RadioNewState(Idle, 10);

      FreqRead = rda5807_GetFreq_In100Khz(I2Cx);
      ShowFreq(FreqRead);
      break;
    }
    
    // ������������ ����� ��������� ����� �� ��������� ������� ���������
    if (StateTickNum)
      StateTickNum--;
    if (!StateTickNum)  // ����� ������ � ������� ��������� �������
    {
      if (RadioState != Idle)   // ������������ � ��������� ��������?
      {     
        // ������ �� rda5807 � ������� �� ��������� ������� �������
        FreqRead = rda5807_GetFreq_In100Khz(I2Cx);
        ShowFreq(FreqRead);
      }

      RadioNewState(Idle, 10);
      Freq100hz = 0;
    }

    // ����������� �������� ��������� �����
    delay_ms(100);
  }
}
//==============================================================================
